<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HELPDESK SYSTEM CENTER</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    
    <link rel="shortcut icon" href="{{asset("lti-pdm/media/images/favicon.ico")}}">
    
    <link href="{{asset("lti-pdm/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
    
    <link href="{{asset("lti-pdm/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset("lti-pdm/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="{{asset("lti-pdm/plugins/iCheck/square/blue.css")}}" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <?php
                $company = App\Model\helpdesk\Settings\Company::where('id', '=', '1')->first();
        $system = App\Model\helpdesk\Settings\System::where('id', '=', '1')->first();
        ?>
        @if($system->url)
          <a href="{!! $system->url !!}" rel="home">
        @else
          <a href="{{url('/')}}" rel="home">
        @endif
              <br>
<!--
                <div id="logo-app" class="site-logo2 text-center">
                    <img src="{{asset('uploads/company')}}{{'/'}}helpdesk.png" alt="User Image" width="90px" height="90px"/>
                </div>
                @if($company->use_logo == 1)
                  <img src="{{asset('uploads/company')}}{{'/'}}{{$company->logo}}" alt="User Image" width="200px" />
                @else
                  @if($system->name)
                    {!! $system->name !!}
                  @else
                    <b>SUPPORT</b> CENTER
                  @endif
                @endif
-->
                </a>
      </div><!-- /.login-logo -->
        
    <div class="login-header">&nbsp;</div>
        <div class="login-breadcrumbs">
            <i class="fa fa-key"></i> Login
        </div>
          
    <div class="login-box-body">                
         
            
            @yield('body')
    </div><!-- /.login-box -->
    <div class="login-box-msg">
    </br>
      <p class="text-muted2"> &copy; <a href="#" class="login-link">{!! $company->company_name !!}</a> {!! Lang::get('lang.all_rights_reserved') !!}. </p>
    </div>
    </div>

    <script src="{{asset("lti-pdm/js/ajax-jquery.min.js")}}" type="text/javascript"></script>

    <script src="{{asset("lti-pdm/js/bootstrap.min.js")}}" type="text/javascript"></script>

    <!-- iCheck -->
    <script src="{{asset("lti-pdm/plugins/iCheck/icheck.min.js")}}" type="text/javascript"></script>

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

  </body>
</html>
